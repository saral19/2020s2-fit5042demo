package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial
 * instructions. This is the main driver class. This class contains the main
 * method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Saral
 */
public class RealEstateAgency {
	private String name;
	private final PropertyRepository propertyRepository;

	public RealEstateAgency(String name) throws Exception {
		this.name = name;
		this.propertyRepository = PropertyRepositoryFactory.getInstance();
	}

	// this method is for initializing the property objects
	// complete this method
	public void createProperty() throws Exception {
		Property property1 = new Property(24, "Boston Avenue", 2, 150, 420000);
		Property property2 = new Property(11, "Bettina Street", 3, 352, 360000);
		Property property3 = new Property(3, "Wattle Avenue", 5, 800, 650000);
		Property property4 = new Property(4, "Hamilton Street", 2, 170, 435000);
		Property property5 = new Property(82, "Spring Road", 1, 60, 820000);
		propertyRepository.addProperty(property1);
		propertyRepository.addProperty(property2);
		propertyRepository.addProperty(property3);
		propertyRepository.addProperty(property4);
		propertyRepository.addProperty(property5);
		System.out.println("5 properties have been added successfuly");

	}

	// this method is for displaying all the properties
	// complete this method
	public void displayProperties() throws Exception {
		List<Property> listProperty = propertyRepository.getAllProperties();
		for (Property property : listProperty) {
			System.out.println(property.toString());
		}
	}

	// this method is for searching the property by ID
	// complete this method
	public void searchPropertyById() throws Exception {
		System.out.println("Enter the id of the property you want to search");
		Scanner sc = new Scanner(System.in);
		int id = sc.nextInt();
		Property returnedProperty = propertyRepository.searchPropertyById(id);
		if (returnedProperty == null) {
			System.out.println("Property not found");
		} else {
			System.out.println(returnedProperty.toString());
		}
	}

	public void run() throws Exception {
		createProperty();
		System.out.println("********************************************************************************");
		displayProperties();
		System.out.println("********************************************************************************");
		searchPropertyById();
	}

	public static void main(String[] args) {
		try {
			new RealEstateAgency("FIT5042 Real Estate Agency").run();
		} catch (Exception ex) {
			System.out.println("Application fail to run: " + ex.getMessage());
		}
	}
}
